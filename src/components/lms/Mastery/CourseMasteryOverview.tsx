import React, { PureComponent } from "react";
import { useState, useEffect } from "react";
import { Box, Center, Grid, GridItem, Heading, ListItem, Text, UnorderedList } from "@chakra-ui/react";
import { ContributorRecord } from "@/src/types/contributor";
import { hexToString } from "@/src/utils";

import {
  PieChart,
  Pie,
  Sector,
  Cell,
  ResponsiveContainer,
  LabelList,
  BarChart,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
} from "recharts";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042", "#FE8800", "#FE0088"];

// Create a mastery type - or pull from somewhere else

export const CourseMasteryOverview = () => {
  const [currentData, setCurrentData] = useState<any[]>([]);
  const [masteryStatusList, setMasteryStatusList] = useState<ContributorRecord[]>([]);
  const [masteryCountList, setMasteryCountList] = useState<number[]>([]);
  const [loading, setLoading] = useState(true);
  const [luckyNumberData, setLuckyNumberData] = useState([
    { name: "changed", value: 0 },
    { name: "unchanged", value: 0 },
  ]);
  const [highestMasteryLevel, setHighestMasteryLevel] = useState([
    { name: "103", students: 0 },
    { name: "201", students: 0 },
    { name: "202", students: 0 },
    { name: "203", students: 0 },
    { name: "204", students: 0 },
  ]);

  const [surveyCompletion, setSurveyCompletion] = useState([
    { name: "complete", value: 17 },
    { name: "not complete", value: 136 },
  ]);

  const [preferredLanguage, setPreferredLanguage] = useState([
    { name: "Aiken", value: 1 },
    { name: "Plu-TS + Aiken", value: 1 },
    { name: "Opshin", value: 1 },
    { name: "Undecided", value: 2 },
    { name: "Plutus Tx", value: 12 },
  ]);

  const [preferredRole, setPreferredRole] = useState([
    { name: "All of the Above!", value: 2 },
    { name: "Front-End Dev", value: 1 },
    { name: "Full Stack Dev", value: 1 },
    { name: "Business Analyst", value: 1 },
    { name: "Facilitator / Tinkerer", value: 1 },
    { name: "Figuring it Out", value: 2 },
    { name: "Plutus Dev", value: 9 },
  ]);

  useEffect(() => {
    const fetchData = async () => {
      if (loading && process.env.NEXT_PUBLIC_MAESTRO_KEY) {
        const response1: void | Response = await fetch(
          `https://preprod.gomaestro-api.org/v0/addresses/utxos?count=100&page=1`,
          {
            method: "POST",
            body: JSON.stringify(["addr_test1wr6ewsvtmdjv8znh7wxvw9qezgwvju5rdk9gmgefvrvrhug7zrfe0"]),
            headers: {
              "content-type": "application/json",
              "api-key": process.env.NEXT_PUBLIC_MAESTRO_KEY,
            },
          }
        ).catch((e) => console.error(e));

        const response2: void | Response = await fetch(
          `https://preprod.gomaestro-api.org/v0/addresses/utxos?count=100&page=2`,
          {
            method: "POST",
            body: JSON.stringify(["addr_test1wr6ewsvtmdjv8znh7wxvw9qezgwvju5rdk9gmgefvrvrhug7zrfe0"]),
            headers: {
              "content-type": "application/json",
              "api-key": process.env.NEXT_PUBLIC_MAESTRO_KEY,
            },
          }
        ).catch((e) => console.error(e));

        if (response1 && response2) {
          const jsonData1 = await response1.json();
          const jsonData2 = await response2.json();
          const combined: any[] = [...jsonData1, ...jsonData2];
          setCurrentData(combined);
          setLoading(false);
        }
      }
    };

    if (loading) {
      fetchData();
    }
  }, [loading]);

  useEffect(() => {
    const _data: any[] = [];
    const _result: ContributorRecord[] = [];
    if (currentData.length > 0) {
      {
        currentData.forEach((utxo: any) => _data.push(utxo));
      }
    }

    setLuckyNumberData([
      { name: "changed", value: _data.filter((utxo) => utxo.datum.json.fields[0].int != 5).length },
      { name: "unchanged", value: _data.filter((utxo) => utxo.datum.json.fields[0].int == 5).length },
    ]);

    const _resultsWithMastery = _data.filter((utxo) => utxo.datum.json.fields[1].list.length > 0);
    _resultsWithMastery.forEach((contributorUTxO: any) => {
      if (contributorUTxO.assets.length > 1) {
        const _record: ContributorRecord = {
          alias: hexToString(contributorUTxO.assets[1].unit.substring(63)),
          contributorDatum: contributorUTxO.datum.json,
        };
        _result.push(_record);
      }
    });
    setMasteryStatusList(_result);
  }, [currentData]);

  useEffect(() => {
    if (masteryStatusList.length > 0) {
      const _masteryLists: string[][] = [];
      const counts: any = {};
      masteryStatusList.forEach((contrib: ContributorRecord) => {
        const _mastery = contrib.contributorDatum.fields[1].list.map((module) => hexToString(module.bytes));
        _masteryLists.push(_mastery);
      });
      for (const _mastery of _masteryLists) {
        for (const item of _mastery) {
          if (counts[item]) {
            counts[item]++;
          } else {
            counts[item] = 1;
          }
        }
      }
      console.log("Contrib Mastery:", counts);
      setHighestMasteryLevel([
        { name: "103", students: counts["Module103 with GitLab"] + counts["Module103 no GitLab"] },
        { name: "201", students: counts["Module201"] },
        { name: "202", students: counts["Module202"] },
        { name: "203", students: counts["Module203"] },
        { name: "204", students: counts["Module204"] },
      ])
    }
  }, [masteryStatusList]);

  return (
    <Box>
      <Grid py="5" templateColumns={{ base: "repeat(1, 1fr)", lg: "repeat(3, 1fr)" }} gap={5}>
        <Center flexDirection="column" border="1px">
          <Heading size="4xl">{currentData.length}</Heading>
          <Heading size="xl">PPBL Tokens Minted</Heading>
        </Center>
        <Center flexDirection="column" border="1px">
          <Heading>Module Completion</Heading>
          <BarChart
            width={350}
            height={300}
            data={highestMasteryLevel}
            margin={{
              top: 5,
              right: 5,
              left: -10,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="students" fill="#8884d8" />
          </BarChart>
        </Center>
        <Center flexDirection="column" border="1px">
          <Heading>Lucky Numbers</Heading>
          <PieChart width={300} height={300}>
            <Pie
              data={luckyNumberData}
              cx={150}
              cy={120}
              innerRadius={30}
              outerRadius={120}
              fill="theme.orange"
              paddingAngle={0}
              dataKey="value"
              label
            >
              <Tooltip />
              {luckyNumberData.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
              ))}
              <LabelList dataKey="name" position="inside" />
            </Pie>
          </PieChart>
        </Center>
        <Center flexDirection="column" border="1px">
          <Heading size="lg">Mid Course Survey Completion</Heading>
          <PieChart width={350} height={300}>
            <Pie
              data={surveyCompletion}
              cx={175}
              cy={120}
              innerRadius={30}
              outerRadius={120}
              fill="theme.orange"
              paddingAngle={0}
              dataKey="value"
              label
            >
              {surveyCompletion.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
              ))}
              <LabelList dataKey="name" position="inside" />
            </Pie>
          </PieChart>
          <Text pb="5" color="theme.yellow" fontWeight="bold">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLScFwXP_6RTBa835JulXgutovJSX1U6W7OHl2i257z3KCqhbzw/viewform">
              Click here to complete the survey
            </a>
          </Text>
        </Center>
        <Center flexDirection="column" border="1px">
          <Heading size="lg" w="70%">
            Favorite Smart Contract Language (so far!)
          </Heading>
          <PieChart width={500} height={300}>
            <Pie
              data={preferredLanguage}
              cx={250}
              cy={150}
              innerRadius={30}
              outerRadius={120}
              fill="theme.orange"
              paddingAngle={0}
              dataKey="value"
            >
              {preferredLanguage.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
              ))}
              <LabelList dataKey="name" position="outside" />
            </Pie>
          </PieChart>
        </Center>
        <Center flexDirection="column" border="1px">
          <Heading size="lg">Preferred Role</Heading>
          <PieChart width={500} height={300}>
            <Pie
              data={preferredRole}
              cx={250}
              cy={150}
              innerRadius={30}
              outerRadius={120}
              fill="theme.orange"
              paddingAngle={0}
              dataKey="value"
            >
              {preferredRole.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
              ))}
              <LabelList dataKey="name" position="outside" />
            </Pie>
          </PieChart>
        </Center>
      </Grid>

      <Grid
        bg="theme.lightGray"
        p="5"
        templateColumns={{ base: "repeat(1, 1fr)", lg: "repeat(3, 1fr)", xl: "repeat(5, 1fr)" }}
        gap={5}
      >
        {masteryStatusList.map((stat: ContributorRecord, index: number) => (
          <Box key={index} bg="theme.light" color="theme.dark" boxShadow="2xl">
            <Box p="2" bg="theme.dark" color="theme.light">
              <Text>{stat.alias}</Text>
            </Box>
            <Box p="3">
              <Text>Lucky #: {stat.contributorDatum.fields[0].int}</Text>
              <UnorderedList>
                {stat.contributorDatum.fields[1].list.map((item: { bytes: string }, index: number) => (
                  <ListItem key={index}>{hexToString(item.bytes)}</ListItem>
                ))}
              </UnorderedList>
            </Box>
          </Box>
        ))}
      </Grid>
    </Box>
  );
};
