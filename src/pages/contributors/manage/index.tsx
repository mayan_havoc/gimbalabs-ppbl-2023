import GPTENav from "@/src/components/gpte/GPTENav";
import { PPBLContext } from "@/src/context/PPBLContext";
import { hexToString } from "@/src/utils";
import { gql, useQuery } from "@apollo/client";
import { Box, Heading, Button, Text, Input, Spacer, Flex } from "@chakra-ui/react";
import Head from "next/head";
import { useContext, useEffect, useState } from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import UpdateProjectList from "@/src/components/gpte/transactions/UpdateProjectList";

export default function ManagePage() {
  const ppblContext = useContext(PPBLContext);
  const [moduleList, setModuleList] = useState<string[] | undefined>(undefined);

  useEffect(() => {
    if (ppblContext.treasuryUTxO?.datum) {
      const _list: string[] = [];
      ppblContext.treasuryUTxO.datum.value.fields[0].list.forEach((item: { bytes: string }) => {
        _list.push(hexToString(item.bytes));
      });
      if (_list) {
        setModuleList(_list);
      }
    }
  }, [ppblContext]);

  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="PPBL Manage Treasury" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <GPTENav />
      <Box w={["95%", "80%"]} mx="auto" my="4em">
        <Heading>Module List</Heading>
        <Box>{JSON.stringify(moduleList)}</Box>
        <Box>{JSON.stringify(ppblContext.treasuryUTxO)}</Box>
        {/* {moduleList && (
          <Formik
            initialValues={{ modules: moduleList }}
            enableReinitialize
            onSubmit={(values) => {
              setModuleList(values.modules);
            }}
            render={({ values }) => (
              <Form>
                <FieldArray
                  name="modules"
                  render={(arrayHelpers) => (
                    <Box p="5" w="50%">
                      {values.modules && values.modules.length > 0 ? (
                        values.modules.map((module: string, index) => (
                          <Flex direction="row" key={index} border="1px" p="5" my="2" color="theme.light">
                            {module ? (
                              <Box>
                                <Text>{module}</Text>
                              </Box>
                            ) : (
                              <Box>
                                <Field name={`modules.${index}`} placeholder="paste new hash" />
                              </Box>
                            )}
                            <Spacer />
                            <Button
                              size="sm"
                              onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                            >
                              -
                            </Button>
                            <Button
                              size="sm"
                              onClick={() => arrayHelpers.insert(index, "")} // insert an empty string at a position
                            >
                              +
                            </Button>
                          </Flex>
                        ))
                      ) : (
                        <>
                          <Button onClick={() => arrayHelpers.push("")}>Add a Module</Button>
                        </>
                      )}
                      <Box>
                        <Button type="submit">Update Treasury Datum</Button>
                      </Box>
                      <UpdateProjectList newModuleList={moduleList} />
                      <Box>{values.modules}</Box>
                    </Box>
                  )}
                />
              </Form>
            )}
          />
        )} */}
      </Box>
    </>
  );
}
