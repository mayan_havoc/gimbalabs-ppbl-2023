import { Heading, Box, Text, Table, TableContainer, Thead, Th, Tbody, Tr, Td, Link as CLink } from "@chakra-ui/react";
import Head from "next/head";

import events from "@/src/data/live-coding/events-english.json";
import Link from "next/link";

export default function LiveCoding() {
  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="Gimbalabs Live Coding Sessions" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={["95%"]} mx="auto">
        <Heading py="10" size="2xl">
          Plutus PBL Live Coding Sessions:
        </Heading>

        <Heading>Gimbalabs - English</Heading>
        <Text fontSize="xl" w="80%" my="3">
          Updated 2023-08-04
        </Text>
        <Text fontSize="xl" w="80%" my="3">
          Plutus PBL Live Coding will not meet on 2023-08-09, 08-10, 08-16, and 08-17. We&apos;ll be back for the
          regular schedule on 2023-08-23, and continue through the rest of 2023.
        </Text>
        <Text fontSize="xl" w="80%" my="3">
          Our current goal is to support as many students as possible to complete the 200-Level, &quot;Building
          Background Knowledge&quot; modules, including{" "}
          <CLink href="/modules/204/slts">Module 204: The PPBL Faucet Project</CLink>. Check out the recorded sessions of Live Coding on 2023-08-03 and
          08-04, where we took a closer look at the PPBL Faucet Project, highlighting some key concepts and making
          connections to earlier modules.
        </Text>
        <Text fontSize="xl" w="80%" my="3">
          In September and beyond, we will continue to support students to complete the 200-Level modules, and launch
          the next phase of Plutus PBL: creating new opportunities for you to contribute, as we roll out the 300-Level
          modules.
        </Text>
        <Heading size="md" py="5">
          Upcoming:
        </Heading>
        <TableContainer pb="5">
          <Table size="sm">
            <Thead>
              <Tr>
                <Th>Date</Th>
                <Th>Time</Th>
                <Th>Title</Th>
                <Th>Description</Th>
                <Th>Register</Th>
              </Tr>
            </Thead>
            <Tbody>
              {events.events
                .filter((ev: any) => !ev.complete)
                .map((event: any, i) => (
                  <Tr key={i}>
                    <Td>{event.date}</Td>
                    <Td>{event.time}</Td>
                    <Td>{event.title}</Td>
                    {event.links && event.links[0] ? (
                      <Td color="theme.yellow">
                        <Link href={event.links[0]}>{event.description}</Link>
                      </Td>
                    ) : (
                      <Td>{event.description}</Td>
                    )}
                    <Td>
                      <CLink href={event.regLink} target="_blank" color="theme.green">
                        Registration Link
                      </CLink>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
        <Heading size="md" py="5">
          Past Meetings:
        </Heading>
        <TableContainer pb="10">
          <Table size="sm">
            <Thead>
              <Tr>
                <Th>Date</Th>
                <Th>Title</Th>
                <Th>Description</Th>
                <Th>Recording</Th>
              </Tr>
            </Thead>
            <Tbody>
              {events.events
                .filter((ev: any) => ev.complete)
                .map((event: any, i) => (
                  <Tr key={i}>
                    <Td>{event.date}</Td>
                    <Td>{event.title}</Td>
                    {event.links && event.links[0] ? (
                      <Td color="theme.yellow">
                        <Link href={event.links[0]}>{event.description}</Link>
                      </Td>
                    ) : (
                      <Td>{event.description}</Td>
                    )}
                    <Td>
                      <CLink href={event.recordingLink} target="_blank" color="theme.yellow">
                        View Meeting Recording
                      </CLink>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
        {/* <Divider py="5" />
        <Heading>Gimbalabs Indonesia</Heading>
        <Divider py="5" />
        <Heading>Gimbalabs Vietnam</Heading> */}
      </Box>
    </>
  );
}
